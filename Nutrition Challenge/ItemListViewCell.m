//
//  ItemListViewCell.m
//  Nutrition Challenge
//
//  Created by dudemaster4 on 15/11/17.
//  Copyright © 2017 Robert Mangion. All rights reserved.
//

#import "ItemListViewCell.h"

@implementation ItemListViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initCell:(FoodObject *)item
{
   self.name.text = item.item_name;
   self.calories.text = [NSString stringWithFormat:@"%@", item.nf_calories];
   self.fat.text = [NSString stringWithFormat:@"%@", item.nf_total_fat];
}

@end
