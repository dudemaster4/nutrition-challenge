//
//  FoodObject.h
//  Nutrition Challenge
//
//  Created by dudemaster4 on 15/11/17.
//  Copyright © 2017 Robert Mangion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodObject : NSObject

@property(nonatomic, strong) NSString *item_name;
@property(nonatomic, strong) NSString *nf_calories;
@property(nonatomic, strong) NSString *nf_total_fat;

@end
