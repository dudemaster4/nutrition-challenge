//
//  NutritionViewController.m
//  Nutrition Challenge
//
//  Created by dudemaster4 on 15/11/17.
//  Copyright © 2017 Robert Mangion. All rights reserved.
//

#import "NutritionViewController.h"
#import "FoodObject.h"
#import "ItemListViewCell.h"

@interface NutritionViewController ()

@end

@implementation NutritionViewController

#define SEARCH_VIEW_ROW_HEIGHT 44
#define LIST_VIEW_ROW_HEIGHT 95

NSString *endPoint = @"https://api.nutritionix.com/v1_1/search/";
NSString *keys = @"?fields=item_name,nf_calories,nf_total_fat&appId=1&appKey=2";

- (void)viewDidLoad
{
    [super viewDidLoad];
   
   self.searchView.hidden = YES;
   self.itemList = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
   if([searchText isEqualToString:@""] || searchText == nil)
   {
      self.searchView.hidden = YES;
      [self.searchBar setBackgroundColor: [UIColor clearColor]];
   }
   else
   {
      [self.searchList removeAllObjects];
      
      if(![self.searchBar.text isEqualToString:@""] && self.searchBar.text != nil)
      {
         self.searchView.hidden = NO;
         [self.searchView reloadData];
      }
   }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   if(self.searchList.count == 0)
      self.searchView.hidden = YES;
   else self.searchView.hidden = NO;
   
   if(tableView == self.searchView)
      return self.searchList.count;
   else
      return self.itemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   if(tableView == self.searchView)
   {
      UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchCell" forIndexPath:indexPath];
      
      cell.textLabel.text = [self.searchList objectAtIndex:indexPath.row].item_name;
      return cell;
   }
   else
   {
      ItemListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listCell" forIndexPath:indexPath];
      
      [cell initCell: [self.itemList objectAtIndex:indexPath.row]];
      return cell;
   }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   if(tableView == self.searchView)
      return SEARCH_VIEW_ROW_HEIGHT;
   else return LIST_VIEW_ROW_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   [self.itemList addObject: [self.searchList objectAtIndex:indexPath.row]];
   
   self.calories += [[self.searchList objectAtIndex:indexPath.row].nf_calories doubleValue];
   self.totalCalories.text = [NSString stringWithFormat:@"%f", self.calories];
   
   self.fat += [[self.searchList objectAtIndex:indexPath.row].nf_total_fat doubleValue];
   self.totalFat.text = [NSString stringWithFormat:@"%f", self.fat];
   
   [self.listView reloadData];
   
   self.searchView.hidden = YES;
}

- (IBAction)search:(id)sender
{
   NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
   
   [request setHTTPMethod:@"GET"];
   [request setURL: [NSURL URLWithString: [NSString stringWithFormat:@"%@%@%@", endPoint, self.searchBar.text, keys]]];
   
   NSError *err;
   
   NSURLResponse *response;
   
   NSData *responseData = [NSURLConnection sendSynchronousRequest:request   returningResponse:&response error:&err];
   
   NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &err];
   
   self.searchList = [[NSMutableArray<FoodObject *> alloc] init];
   
   NSArray *eventsArray = [[NSArray alloc] init];
   eventsArray = [jsonArray objectForKey: @"hits"];
   
   for(NSDictionary *dictionary in eventsArray)
   {
      FoodObject *item = [[FoodObject alloc] init];
      
      item.item_name = [[dictionary objectForKey:@"fields"] valueForKey:@"item_name"];
      item.nf_calories = [[dictionary objectForKey:@"fields"] valueForKey:@"nf_calories"];
      item.nf_total_fat = [[dictionary objectForKey:@"fields"] valueForKey:@"nf_total_fat"];
      
      if(item)
         [self.searchList addObject: item];
   }
   
   [self.searchView reloadData];
}

@end
