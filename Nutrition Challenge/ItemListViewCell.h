//
//  ItemListViewCell.h
//  Nutrition Challenge
//
//  Created by dudemaster4 on 15/11/17.
//  Copyright © 2017 Robert Mangion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodObject.h"

@interface ItemListViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *calories;
@property (strong, nonatomic) IBOutlet UILabel *fat;

- (void)initCell:(FoodObject *)item;

@end
