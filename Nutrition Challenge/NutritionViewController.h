//
//  NutritionTableViewController.h
//  Nutrition Challenge
//
//  Created by dudemaster4 on 15/11/17.
//  Copyright © 2017 Robert Mangion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodObject.h"

@interface NutritionViewController : UIViewController

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *searchView;
@property (strong, nonatomic) IBOutlet UITableView *listView;
@property (strong, nonatomic) NSMutableArray<FoodObject*> *searchList;
@property (strong, nonatomic) NSMutableArray<FoodObject*> *itemList;
@property (strong, nonatomic) IBOutlet UILabel *totalCalories;
@property (strong, nonatomic) IBOutlet UILabel *totalFat;
@property (assign, nonatomic) double calories;
@property (assign, nonatomic) double fat;

- (IBAction)search:(id)sender;

@end
