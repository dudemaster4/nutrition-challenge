//
//  AppDelegate.h
//  Nutrition Challenge
//
//  Created by dudemaster4 on 15/11/17.
//  Copyright © 2017 Robert Mangion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

